#!/bin/bash

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/opt/aws/bin

function removehost_etc_hosts() {
    RM_HOSTNAME=$1
    if grep -q "${RM_HOSTNAME}" /etc/hosts
    then
        echo "${RM_HOSTNAME} Found in your /etc/hosts, Removing now..."
        sed -i".bak" "/${RM_HOSTNAME}/d" /etc/hosts
    else
        echo "${RM_HOSTNAME} was not found in your /etc/hosts"
    fi
}

function addhost_etc_hosts() {
    SET_HOSTNAME=$1
    HOSTS_LINE="${MY_IP}\t${SET_HOSTNAME}\t${MY_SHORT_HOSTNAME}"
    if grep -q "$SET_HOSTNAME" /etc/hosts
        then
            echo "${SET_HOSTNAME} already exists : " + $(grep "${SET_HOSTNAME}" /etc/hosts)
        else
            echo "Adding ${SET_HOSTNAME} to your /etc/hosts"
            echo -e "${HOSTS_LINE}" >> /etc/hosts
            if grep -q "${SET_HOSTNAME}" /etc/hosts
                then
                    echo "${SET_HOSTNAME} was added succesfully: " + $(grep "${SET_HOSTNAME}" /etc/hosts)
                else
                    echo "Failed to Add ${SET_HOSTNAME}, Try again!"
            fi
    fi
}

mkdir -p /root/.ssh
cp -f /opt/.id_rsa /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
cp -f /opt/.id_rsa.pub /root/.ssh/id_rsa.pub
chmod 644 /root/.ssh/id_rsa.pub

mkdir -p /root/.aws
cp -f /opt/.aws-config /root/.aws/config
cp -f /opt/.aws-credentials /root/.aws/credentials
cp -f /opt/.s3cfg /root/.s3cfg

echo "----[ Find IP ]----"
MY_IP=$( ifconfig ens5|head -2|tail -1|awk '{print $2}' )
if [[ ${MY_IP} = *":"* ]]; then
  MY_IP=$( ifconfig ens5|head -2|tail -1|awk '{print $2}'|awk -F: '{print $2}' )
fi
if [[ -z "${MY_IP}" ]]; then
    echo "[FATAL] Unable to find IP on adapter /dev/eth0"
    exit 4
fi
echo "MY_IP: ${MY_IP}"
echo

INSTANCE_ID=$( ec2metadata --instance-id )

KEY="Environment"
ENV_VALUE=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$KEY" --output=text | cut -f5)
KEY="HOST"
HOST_VALUE=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$KEY" --output=text | cut -f5)
echo "Instance ID: ${INSTANCE_ID} Environment: ${ENV_VALUE} Host: ${HOST_VALUE}"

MY_HOSTNAME=${HOST_VALUE}
environment=${ENV_VALUE}

if [[ -z "${environment}" ]]; then echo missing environment; exit 11; fi;

MY_SHORT_HOSTNAME=$( echo "${MY_HOSTNAME}" | awk -F. '{print $1}' )
if [[ -z "${MY_SHORT_HOSTNAME}" ]]; then
    echo "[FATAL] Unable to find host short name"
    exit 3
fi
MY_SHORT_HOSTNAME_OLD=$( echo "${HOSTNAME}" | awk -F. '{print $1}' )
echo "MY_SHORT_HOSTNAME: ${MY_SHORT_HOSTNAME}"
echo

echo "----[ Update HOSTNAME ]----"
echo "Remove old hostname from /etc/hosts"
removehost_etc_hosts "${HOSTNAME}"
removehost_etc_hosts "${MY_SHORT_HOSTNAME_OLD}"
echo "Add new hostname to /etc/hosts"
addhost_etc_hosts "${MY_HOSTNAME}"
echo "Set new hostname via hostname"
hostname "${MY_HOSTNAME}"

# Find hostname file to modify
if [ -e /etc/sysconfig/networking ]; then
    echo "Write new hostname to /etc/sysconfig/networking"
        # Update HOSTNAME in /etc/sysconfig/network
        if ! grep -q HOSTNAME /etc/sysconfig/network; then
                # Add HOSTNAME entry to /etc/sysconfig/network because one does not exist
                echo "HOSTNAME=${MY_HOSTNAME}" >> /etc/sysconfig/network
        else
                # Modify HOSTNAME
                perl -pi -e 's/HOSTNAME=.*\b/HOSTNAME=${MY_HOSTNAME}/g' /etc/sysconfig/network
        fi
elif [ -e  /etc/hostname ]; then
    echo "Write new hostname to /etc/hostname"
    echo "${MY_HOSTNAME}" > /etc/hostname
else
   echo "[FATAL] Unable to find file to permanently set hostname"
   exit 6
fi

HAVE_ENV='false'
    case "$environment" in
        UAT ) HAVE_ENV='true';;
                Dev ) HAVE_ENV='true';;
        QA ) HAVE_ENV='true';;
        Stage ) HAVE_ENV='true';;
        Prod ) HAVE_ENV='true';;
    esac
if [[ ${HAVE_ENV} == 'false' ]]; then echo "failed to get valid environment [UAT|Dev|QA|Stage|Prod]"; exit 112; fi

echo
echo "! Remember to add this host to DNS !"
echo
echo "HOST: ${MY_HOSTNAME}"
echo "IP: ${MY_IP}"
echo