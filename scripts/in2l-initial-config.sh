#!/bin/bash

USER=$1
IP=$2
ENV=$3
GROUP=$4
ADMIN_PASS=$5

echo "##########################################"
echo "          COPYING FILES TO SERVER         "
echo $ADMIN_PASS > admin-password.txt
scp -r ./admin-password.txt $USER@$IP:/home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt
rm ./admin-password.txt
scp -r ./* $USER@$IP:/home/$USER/ansible/ansible-control-node
echo "##########################################"
echo ""

ssh -q $USER@$IP << EOF
    echo "##########################################"
    echo "      RUNNING PLAYBOOK $PLAYBOOK"
    docker exec ansible-control-node ansible-vault decrypt /ansible/vault/passwords/$ENV-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt
    docker exec ansible-contorl-node ansible-vault decrypt /ansible/vault/config/$ENV/ssh/id_rsa --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault decrypt /ansible/vault/config/$ENV/ssh/id_rsa.pub --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault decrypt /ansible/vault/config/$ENV/config/aws-config --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault decrypt /ansible/vault/config/$ENV/config/aws-credentials --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault decrypt /ansible/vault/config/$ENV/config/s3cfg --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-control-node ansible-playbook -i /ansible/vault/config/$ENV/inventory -l $GROUP --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt /ansible/playbooks/initial-config.yml --ssh-common-args='-o StrictHostKeyChecking=no'
    docker exec ansible-control-node ansible-playbook -i /ansible/vault/config/$ENV/inventory -l $GROUP --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt /ansible/playbooks/ssm-agent.yml --ssh-common-args='-o StrictHostKeyChecking=no'
    docker exec ansible-control-node ansible-playbook -i /ansible/vault/config/$ENV/inventory -l $GROUP --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt /ansible/playbooks/aws-cli.yml --ssh-common-args='-o StrictHostKeyChecking=no'
    docker exec ansible-control-node ansible-playbook -i /ansible/vault/config/$ENV/inventory -l $GROUP --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt /ansible/playbooks/run-in2l-install.yml --ssh-common-args='-o StrictHostKeyChecking=no'
    docker exec ansible-control-node ansible-playbook -i /ansible/vault/config/$ENV/inventory -l $GROUP --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt /ansible/playbooks/user-in2ldevops.yml --ssh-common-args='-o StrictHostKeyChecking=no'
    docker exec ansible-control-node ansible-vault encrypt /ansible/vault/passwords/$ENV-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt
    docker exec ansible-contorl-node ansible-vault encrypt /ansible/vault/config/$ENV/ssh/id_rsa --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault encrypt /ansible/vault/config/$ENV/ssh/id_rsa.pub --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault encrypt /ansible/vault/config/$ENV/config/.aws-config --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault encrypt /ansible/vault/config/$ENV/config/.aws-credentials --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    docker exec ansible-contorl-node ansible-vault encrypt /ansible/vault/config/$ENV/config/.s3cfg --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/passwords/$ENV-password.txt
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/config/$ENV/ssh/id_rsa
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/config/$ENV/ssh/id_rsa.pub
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/config/$ENV/config/.aws-config
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/config/$ENV/config/.aws-credentials
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/config/$ENV/config/.s3cfg
    rm /home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt
    echo "##########################################"
    echo ""
EOF