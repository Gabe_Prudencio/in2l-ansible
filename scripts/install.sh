#!/bin/bash

USER=$1
IP=$2
PORT=$3
ADMIN_PASS=$4

echo "##########################################"
echo "          COPYING FILES TO SERVER         "
echo $ADMIN_PASS > admin-password.txt
scp -r ./admin-password.txt $USER@$IP:/home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt
scp -r ./* $USER@$IP:/home/$USER/ansible/ansible-control-node
echo "##########################################"
echo ""

ssh -q $USER@$IP << EOF

    echo "##########################################"
    echo "                 DOCKER RM                "
    docker stop -t0 ansible-control-node || true
    docker rm ansible-control-node || true
    echo "##########################################"
    echo ""

    echo "##########################################"
    echo "                DOCKER BUILD              "
    cd ansible/ansible-control-node
    docker build -t ansible-image -f Dockerfile .
    echo "##########################################"
    echo ""

    echo "##########################################"
    echo "                 DOCKER RUN               "
    docker run -d -v /home/$USER/ansible/ansible-control-node/playbooks:/ansible/playbooks -v /home/$USER/ansible/ansible-control-node/scripts:/ansible/scripts -v /home/$USER/ansible/ansible-vault/vault:/ansible/vault -v /home/$USER/ansible/ansible-control-node/ssh/id_rsa:/root/.ssh/id_rsa  -p $PORT:80 --name ansible-control-node ansible-image
    echo "##########################################"
    echo ""

    echo "##########################################"
    echo "                 DOCKER EXEC              "
    echo "              (LIST ALL HOSTS)            "
    docker exec ansible-control-node chmod 600 /root/.ssh/id_rsa
    docker exec ansible-control-node ansible-vault decrypt /ansible/vault/passwords/qa-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt
    docker exec ansible-control-node ansible-inventory -i /ansible/vault/config/qa-config/inventory --vault-id qa@/ansible/vault/passwords/qa-password.txt --list
    docker exec ansible-control-node ansible-vault encrypt /ansible/vault/passwords/qa-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/passwords/qa-password.txt
    rm /home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt
    echo "##########################################"
    echo ""
EOF
