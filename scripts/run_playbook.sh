#!/bin/bash 

USER=$1
IP=$2
ENV=$3
GROUP=$4
PLAYBOOK=$5
ADMIN_PASS=$6

echo "###########################################"
echo "              JOB INFORMATION"
echo "------------------------------------------"
echo "EVIRONMENT:" $ENV
echo "GROUP:" $GROUP
echo "PLAYBOOK:" $PLAYBOOK
echo "------------------------------------------"
echo "##########################################"

echo "##########################################"
echo "          COPYING FILES TO SERVER         "
echo $ADMIN_PASS > admin-password.txt
scp -r ./admin-password.txt $USER@$IP:/home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt
rm ./admin-password.txt
scp -r ./* $USER@$IP:/home/$USER/ansible/ansible-control-node
echo "##########################################"
echo ""

ssh -q $USER@$IP << EOF
    echo "##########################################"
    echo "      RUNNING PLAYBOOK $PLAYBOOK"
    docker exec ansible-control-node ansible-vault decrypt /ansible/vault/passwords/$ENV-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt
    docker exec ansible-control-node ansible-playbook -i /ansible/vault/config/$ENV-config/inventory -l $GROUP --vault-id $ENV@/ansible/vault/passwords/$ENV-password.txt /ansible/playbooks/$PLAYBOOK.yml --ssh-common-args='-o StrictHostKeyChecking=no'
    docker exec ansible-control-node ansible-vault encrypt /ansible/vault/passwords/$ENV-password.txt --vault-id admin@/ansible/vault/passwords/admin-password.txt
    sudo chown $USER:$USER /home/$USER/ansible/ansible-vault/vault/passwords/$ENV-password.txt
    rm /home/$USER/ansible/ansible-vault/vault/passwords/admin-password.txt
    echo "##########################################"
    echo ""
EOF
